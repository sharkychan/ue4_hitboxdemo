// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DemoCharacter.h"
#include "Interactable.generated.h"

UCLASS()
class HITBOXDEMO_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Character Actions")
		void beginCharacterInteraction(ECharacterStrikeEnum strikeType);

	UFUNCTION(BlueprintImplementableEvent, Category = "Character Actions")
		void endCharacterInteraction();

	FORCEINLINE class UCapsuleComponent * GetInteractableComponent() const { return InteractableComponent; }


protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UCapsuleComponent * InteractableComponent;

	UFUNCTION(BlueprintCallable, Category = "Interactable")
		void doSomething();
	
};
