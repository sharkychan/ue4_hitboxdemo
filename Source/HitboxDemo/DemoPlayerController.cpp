// Fill out your copyright notice in the Description page of Project Settings.

#include "HitboxDemo.h"
#include "DemoCharacter.h"
#include "DemoPlayerController.h"

ADemoPlayerController::ADemoPlayerController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void ADemoPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	//Set up gameplay keybindings for all player controllers
	InputComponent->BindAxis("moveForward", this, &ADemoPlayerController::moveForward);
	InputComponent->BindAxis("moveSide", this, &ADemoPlayerController::moveSide);
	InputComponent->BindAction("jump", IE_Pressed, this, &ADemoPlayerController::jumpStarted);
	InputComponent->BindAction("jump", IE_Released, this, &ADemoPlayerController::jumpEnded);
	InputComponent->BindAction("light_attack", IE_Pressed, this, &ADemoPlayerController::lightAttackStarted);
	InputComponent->BindAction("light_attack", IE_Released, this, &ADemoPlayerController::lightAttackEnded);
	InputComponent->BindAction("heavy_attack", IE_Pressed, this, &ADemoPlayerController::heavyAttackStarted);
	InputComponent->BindAction("heavy_attack", IE_Released, this, &ADemoPlayerController::heavyAttackEnded);
	InputComponent->BindAction("special", IE_Pressed, this, &ADemoPlayerController::specialStarted);
	InputComponent->BindAction("special", IE_Released, this, &ADemoPlayerController::specialEnded);

}

void ADemoPlayerController::moveForward(float value) {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->moveForward(value);
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::moveSide(float value) {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->moveSide(value);
	}
	else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::jumpStarted() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->jumpStarted();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::jumpEnded() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->jumpEnded();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}


void ADemoPlayerController::lightAttackStarted() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->lightAttackStarted();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::lightAttackEnded() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->lightAttackEnded();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::heavyAttackStarted() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->heavyAttackStarted();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::heavyAttackEnded() {
	auto character = this->GetCharacter();

	if (character->IsA(ADemoCharacter::StaticClass())) {
		auto demoCharacter = Cast<ADemoCharacter>(character);
		demoCharacter->heavyAttackEnded();
	} else {
		//Shouldn't have gotten here!
		if (GEngine) {
			GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("character controller not connected"));
		}
	}
}

void ADemoPlayerController::specialStarted() {

}

void ADemoPlayerController::specialEnded() {

}