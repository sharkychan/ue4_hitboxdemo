// Fill out your copyright notice in the Description page of Project Settings.

#include "HitboxDemo.h"
#include "HitboxDemoStatics.h"


const float	UHitboxDemoStatics::frameDelta = 0.016f;

UHitboxDemoStatics::UHitboxDemoStatics(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {

}

int UHitboxDemoStatics::sgn(float value) {
	return (0.f < value) - (value < 0.f);
}