// Fill out your copyright notice in the Description page of Project Settings.

#include "HitboxDemo.h"
#include "HitboxDemoStatics.h"
#include "Interactable.h"
#include "DemoCharacter.h"


// Sets default values
ADemoCharacter::ADemoCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	//Set default character movements
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	//Set up hitboxes	
	strike1HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("strike1HitBox"));
	strike1HitBox->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);

	strike2SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("strike2SpringArm"));
	strike2SpringArm->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	strike2SpringArm->bDoCollisionTest = false; //spring arm does not have collision
	strike2SpringArm->RelativeRotation = FRotator(0.0f, 180.0f, 0.0f);
	strike2SpringArm->TargetArmLength = 150.0f;

	strike2HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("strike2HitBox"));
	strike2HitBox->AttachToComponent(strike2SpringArm, FAttachmentTransformRules::KeepRelativeTransform, USpringArmComponent::SocketName);
	strike2HitBoxPosition = FVector(20.f, 0.0, 0.0);
	strike2HitBox->SetRelativeLocation(strike2HitBoxPosition);

	//Find DemoCamera blueprint
	static ConstructorHelpers::FObjectFinder<UBlueprint> playerCameraObj(TEXT("Blueprint'/Game/Blueprints/DemoCameraBP'"));
	if (playerCameraObj.Object != NULL) {
		playerCameraBP = (UClass *)playerCameraObj.Object->GeneratedClass;
	}

	numJumps = 0;
	bHitConnected = false;
}

// Called when the game starts or when spawned
void ADemoCharacter::BeginPlay()
{
	Super::BeginPlay();

	/*
	//Spawn slapdash associated camera for crummier experience :P
	if (playerCamera == NULL) {
		playerCamera = GetWorld()->SpawnActor<ADemoCamera>(playerCameraBP, FVector(-100.f, 0.f, 80.f), FRotator(-30.0, 0.0, 0.0));
		playerCamera->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::KeepRelative, true));

	}
	*/
	
}

// Called every frame
void ADemoCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ADemoCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

/* Implementation of applyMovementFromInput which can apply addMovementInput to the target*/
void ADemoCharacter::applyMovementFromInput(EAxis::Type axis, float scale, bool bForce) {
	if (Controller != NULL && scale != 0.f) {
		const FRotator rotation = Controller->GetControlRotation();
		const FRotator yawRotation(0.f, rotation.Yaw, 0.f);
		const FVector direction = FRotationMatrix(yawRotation).GetUnitAxis(axis);
		AddMovementInput(direction, scale, bForce);
	}
}

/*
Implementation of beginJump, which overrides UE4's built in Jump() method
Demonstration of double jump using jump then launch rather than two jumps
*/
void ADemoCharacter::beginJump() {
	if (numJumps == 0) {
		this->Jump();
	}
	else if (numJumps == 1) {
		GetCharacterMovement()->Velocity.Z = 0.0;
		this->LaunchCharacter(FVector(0.0, 0.0, doubleJumpVelocity), false, false);
	}
	numJumps++;
}

/*
Implementation of endJump
*/
void ADemoCharacter::endJump() {
	numJumps = 0;
}

/*
Implementation of beginStrike
*/
void ADemoCharacter::beginStrike(ECharacterStrikeEnum strikeType, float startupFrames, float hitFrames, float recoverFrames, FVector groundVelocity, FVector airVelocity) {
	if (startupFrames <= 0.0) {
		//Special case delay time of 0, since delegate will not be called if time is 0
		beginHits(strikeType, hitFrames, recoverFrames, groundVelocity, airVelocity);
	}
	else {
		FTimerDelegate strikeDelegate = FTimerDelegate::CreateUObject(this, &ADemoCharacter::beginHits, strikeType, hitFrames, recoverFrames, groundVelocity, airVelocity);
		GetWorld()->GetTimerManager().SetTimer(strikeTimer, strikeDelegate, startupFrames, false);
	}
}

/*
Implementation of beginHits
*/
void ADemoCharacter::beginHits(ECharacterStrikeEnum strikeType, float hitFrames, float recoverFrames, FVector groundVelocity, FVector airVelocity) {

	//Update the character's velocity based on their position on the ground or in the air
	//Character is currently only launched along forward trajectory 
	if (GetCharacterMovement()->IsMovingOnGround()) {
		this->LaunchCharacter(groundVelocity*GetActorForwardVector(), false, false);
	} else {
		this->LaunchCharacter(airVelocity*GetActorForwardVector(), false, false);
	}

	FTimerDelegate strikeDelegate = FTimerDelegate::CreateUObject(this, &ADemoCharacter::checkforHits, strikeType, hitFrames, recoverFrames, uint8(0));
	GetWorld()->GetTimerManager().SetTimer(strikeTimer, strikeDelegate, UHitboxDemoStatics::frameDelta, false);
}

/*
Implementation of checkForHits
*/
void ADemoCharacter::checkforHits(ECharacterStrikeEnum strikeType, float hitFrames, float recoverFrames, uint8 hitTimes) {
	UShapeComponent * hitbox = GetStrike1HitBox();

	switch (strikeType) {
	case ECharacterStrikeEnum::LIGHT:
		break;
	case ECharacterStrikeEnum::HEAVY:
		hitbox = GetStrike2HitBox();
		break;
	case ECharacterStrikeEnum::SPECIAL:
		break;
	}

	if (hitTimes * UHitboxDemoStatics::frameDelta < hitFrames && !bHitConnected) {
		//Timer has not exceeded hitFrame time: continue to check for hits

		if (strikeType == ECharacterStrikeEnum::HEAVY) {
			hitbox->SetRelativeLocation(hitbox->RelativeLocation + FVector(-5.0, 0.0, 0.0));

			//Demonstration of line trace
			TArray<FHitResult> HitResults;
			FVector Start = GetActorLocation();
			FVector End = Start + GetActorForwardVector()*300.f;

			const FName TraceTag(TEXT("Hit Trace"));
			FCollisionQueryParams QueryParams(TraceTag, false, NULL);
			QueryParams.bReturnPhysicalMaterial = false;
			FCollisionResponseParams ResponseParam(ECollisionResponse::ECR_Overlap);

			//Check whether movement will set character outside of climable component area using a line trace
			GetWorld()->LineTraceMultiByChannel(HitResults, Start, End, ECC_WorldDynamic, QueryParams, ResponseParam);
			GetWorld()->DebugDrawTraceTag = TraceTag;

			const FHitResult hit = HitResults.Last();
			if (hit.Actor->IsValidLowLevel() && hit.Actor->IsA(AInteractable::StaticClass())) {
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Blue, TEXT("interactable hit by ray"));
			}

		}

		TArray< AActor *> overlappingActors;
		hitbox->GetOverlappingActors(overlappingActors);

		for (AActor * actor : overlappingActors) {
			//actors should respond to hits if overlapping with an interactable object
			//characters should rebound off hit to interactable object as necessary
			if (actor->IsValidLowLevel() && actor->IsA(AInteractable::StaticClass())) {
				GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("interactable hit by box"));

				auto interactable = Cast<AInteractable>(actor);
				interactable->beginCharacterInteraction(strikeType);
				//this->reboundOnStrike(interactable, this->GetVelocity());
				bHitConnected = true;
			}
		}

		FTimerDelegate strikeDelegate = FTimerDelegate::CreateUObject(this, &ADemoCharacter::checkforHits, strikeType, hitFrames, recoverFrames, uint8(hitTimes + 1));
		GetWorld()->GetTimerManager().SetTimer(strikeTimer, strikeDelegate, UHitboxDemoStatics::frameDelta, false);
	}
	else {
		//Timer has exceeded hitFrame time: stop timer and initiate cooldown
		if (strikeType == ECharacterStrikeEnum::HEAVY) {
			hitbox->SetRelativeLocation(strike2HitBoxPosition);
		}


		GetWorld()->GetTimerManager().ClearTimer(strikeTimer);
		if (recoverFrames <= 0.0) {
			//Special case recovery time of 0, since timer will not be called if time is 0
			endStrike();
		}
		else {
			GetWorld()->GetTimerManager().SetTimer(strikeTimer, this, &ADemoCharacter::endStrike, recoverFrames, false);
		}
	}
}

/* Implementation of endStrike */
void ADemoCharacter::endStrike() {
	bHitConnected = false;

}

/* Implementation of FellOutOfWorld to respawn character*/
void ADemoCharacter::FellOutOfWorld(const class UDamageType & dmgType) {
	FTimerHandle respawnTimer;
	GetWorld()->GetTimerManager().SetTimer(respawnTimer, this, &ADemoCharacter::respawnCharacter, 1.f, false);
}

/* Respawn player at spawn point upon death */
void ADemoCharacter::respawnCharacter() {
	//No need to destroy player character -- just reposition at spawn point
	for (TActorIterator<APlayerStart> ActorItr(GetWorld()); ActorItr; ++ActorItr) {
		APlayerStart * pstart = *ActorItr;
		this->SetActorLocationAndRotation(pstart->GetActorLocation(), pstart->GetActorRotation());
		this->GetCharacterMovement()->Velocity = FVector::ZeroVector;
	}
}