// Fill out your copyright notice in the Description page of Project Settings.

#include "HitboxDemo.h"
#include "DemoCharacter.h"
#include "DemoCamera.h"


// Sets default values
ADemoCamera::ADemoCamera(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create camera components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	CameraSpringArm->bDoCollisionTest = false; //Camera spring arm does not have collision
	CameraSpringArm->TargetArmLength = 500.0f;

	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->ProjectionMode = ECameraProjectionMode::Perspective;
	CameraComponent->AttachTo(CameraSpringArm, USpringArmComponent::SocketName);

	CameraComponent->bAutoActivate = true;	//This is our primary camera
}

// Called when the game starts or when spawned
void ADemoCamera::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADemoCamera::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	//Set camera position and orientation here

	//Set PlayerController to use this camera for view target
	APlayerController * playerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);

	if (playerController != nullptr) {
		playerController->SetViewTargetWithBlend(this);
	}

}

