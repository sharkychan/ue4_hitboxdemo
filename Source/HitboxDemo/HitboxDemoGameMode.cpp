// Skazka and all related IP are copyright Akula Games, LLC.

#include "HitboxDemo.h"
#include "DemoCharacter.h"
#include "DemoPlayerController.h"
#include "HitboxDemoGameMode.h"

AHitboxDemoGameMode::AHitboxDemoGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	//Set default player controller class
	PlayerControllerClass = ADemoPlayerController::StaticClass();

	//Set default pawn class
	DefaultPawnClass = ADemoCharacter::StaticClass();

	static ConstructorHelpers::FClassFinder<APawn> CharacterObj(TEXT("Blueprint'/Game/Blueprints/DemoCharacterBP'"));

	if (CharacterObj.Class != NULL) {
		DefaultPawnClass = CharacterObj.Class;
	}

	//Set GameMode booleans
	bPauseable = true;				//Game is pauseable
	bUseSeamlessTravel = true;		//Travel between maps is seamless
}


