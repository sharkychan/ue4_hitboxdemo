// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "HitboxDemoStatics.generated.h"


/**
 * 
 */
UCLASS()
class HITBOXDEMO_API UHitboxDemoStatics : public UObject
{
	GENERATED_UCLASS_BODY()

public:
	static const float frameDelta;

	static int sgn(float value);

};
