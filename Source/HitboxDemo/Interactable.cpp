// Fill out your copyright notice in the Description page of Project Settings.

#include "HitboxDemo.h"
#include "Interactable.h"


// Sets default values
AInteractable::AInteractable(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	//Create components
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	InteractableComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("InteractableComponent"));
	InteractableComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	GetInteractableComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

// Called when the game starts or when spawned
void AInteractable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractable::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

//Do something...
void AInteractable::doSomething() {
	GEngine->AddOnScreenDebugMessage(-1, 2.f, FColor::Green, TEXT("Interactable does something..."));
}

