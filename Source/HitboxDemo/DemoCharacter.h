// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "DemoCamera.h"
#include "DemoCharacter.generated.h"

UENUM(BlueprintType)
enum class ECharacterStrikeEnum : uint8 {
	LIGHT			UMETA(DisplayName = "Light Hit"),
	HEAVY			UMETA(DisplayName = "Heavy Hit"),
	SPECIAL			UMETA(DisplayName = "Special")
};

UCLASS(Blueprintable, config = Game)
class HITBOXDEMO_API ADemoCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ADemoCharacter(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	virtual void FellOutOfWorld(const class UDamageType & dmgType) override;

	/* Handle controller inputs in Blueprint using Blueprint Implementable Events*/

	/* Character is moving */
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void moveForward(float value);
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void moveSide(float value);

	/* Character jump has started */
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void jumpStarted();
	/* Character jump has finished */
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void jumpEnded();
	/* Character's light attack has started*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void lightAttackStarted();
	/* Character's light attack has finished*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void lightAttackEnded();
	/* Character's heavy attack has started*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void heavyAttackStarted();
	/* Character's heavy attack has finished*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Input Events")
		void heavyAttackEnded();

	FORCEINLINE class UBoxComponent * GetStrike1HitBox() const { return strike1HitBox; }
	FORCEINLINE class USpringArmComponent * GetStrike2SpringArm() const { return strike2SpringArm; }
	FORCEINLINE class UBoxComponent * GetStrike2HitBox() const { return strike2HitBox; }

	FORCEINLINE class ADemoCamera * GetPlayerCamera() const { return playerCamera; }


protected:
	/* Values related to alternate way of double jumping*/
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		uint8 numJumps;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float doubleJumpVelocity;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UClass * playerCameraBP;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ADemoCamera * playerCamera;

	/* Timer that dictates strike timings: delay, hit and recover */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Striking")
		FTimerHandle strikeTimer;
	/* Boolean dictating whether the Character is currently striking an object */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Boxes")
		bool bHitConnected;

	/* Hitboxes used to determine character actions and effects */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Boxes")
		UBoxComponent * strike1HitBox;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Boxes")
		UBoxComponent * strike2HitBox;

	/* Spring arms to attach hitboxes for targeting capabilities */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit Boxes")
		USpringArmComponent * strike2SpringArm;

	/* MOVING */
	/* Update character's movement direction*/
	UFUNCTION(BlueprintCallable, Category = "Movement")
		void applyMovementFromInput(EAxis::Type axis, float scale, bool bForce);

	/* JUMPING */
	/* beginJump sets jump state to allow for double jumping */
	UFUNCTION(BlueprintCallable, Category = "Jumping")
		void beginJump();
	/* endJump sets jump state after jump completed */
	UFUNCTION(BlueprintCallable, Category = "Jumping")
		void endJump();

	/* STRIKING */
	/* beginStrike sets the times for delay, hit and recover and begins the character action
	Delay by delayFrames seconds before initiating the checkforHits method */
	UFUNCTION(BlueprintCallable, Category = "Striking")
		void beginStrike(ECharacterStrikeEnum strikeType, float startupFrames, float hitFrames, float recoverFrames, FVector groundVelocity, FVector airVelocity);
	/* beginHits checks for hits every 16ms for the duration of hitFrames time
	TODO : mark flag based on strikeType, so that players can string together multiple attacks */
	UFUNCTION(Category = "Striking")
		void beginHits(ECharacterStrikeEnum strikeType, float hitFrames, float recoverFrames, FVector groundVelocity, FVector airVelocity);
	/* checkForHits checks for overlapping objects during a given interval in hitFrames time */
	UFUNCTION(Category = "Striking")
		void checkforHits(ECharacterStrikeEnum strikeType, float hitFrames, float recoverFrames, uint8 hitTimes);
	/* endStrike initiates a cool down time during which players cannot input actions */
	UFUNCTION(BlueprintCallable, Category = "Striking")
		void endStrike();

	//Respawn
	UFUNCTION(BlueprintCallable, Category = "Respawn")
		void respawnCharacter();

private:
	FVector strike2HitBoxPosition;

};
