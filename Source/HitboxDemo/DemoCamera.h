// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DemoCamera.generated.h"

UCLASS(Blueprintable)
class HITBOXDEMO_API ADemoCamera : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADemoCamera(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	FORCEINLINE class USpringArmComponent * GetCameraSpringArm() const { return CameraSpringArm; }
	FORCEINLINE class UCameraComponent * GetCameraComponent() const { return CameraComponent; }

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		USpringArmComponent * CameraSpringArm;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
		UCameraComponent * CameraComponent;
	
};
