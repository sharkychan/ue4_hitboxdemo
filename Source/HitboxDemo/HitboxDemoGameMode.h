// Skazka and all related IP are copyright Akula Games, LLC.

#pragma once

#include "GameFramework/GameMode.h"
#include "HitboxDemoGameMode.generated.h"

/**
 * 
 */
UCLASS()
class HITBOXDEMO_API AHitboxDemoGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AHitboxDemoGameMode(const FObjectInitializer& ObjectInitializer);
	
	
};
