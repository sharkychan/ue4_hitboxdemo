// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "DemoPlayerController.generated.h"

/**
 * 
 */
UCLASS(config=Game)
class HITBOXDEMO_API ADemoPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ADemoPlayerController(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
		void moveForward(float value);
	UFUNCTION()
		void moveSide(float value);
	UFUNCTION()
		void jumpStarted();
	UFUNCTION()
		void jumpEnded();
	UFUNCTION()
		void lightAttackStarted();
	UFUNCTION()
		void lightAttackEnded();
	UFUNCTION()
		void heavyAttackStarted();
	UFUNCTION()
		void heavyAttackEnded();
	UFUNCTION()
		void specialStarted();
	UFUNCTION()
		void specialEnded();
	
	
	
	
};
