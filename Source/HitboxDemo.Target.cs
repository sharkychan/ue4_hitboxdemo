// Skazka and all related IP are copyright Akula Games, LLC.

using UnrealBuildTool;
using System.Collections.Generic;

public class HitboxDemoTarget : TargetRules
{
	public HitboxDemoTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "HitboxDemo" } );
	}
}
