### How do I get set up? ###

1) Clone repository from git@bitbucket.org:sharkychan/ue4_hitboxdemo.git 
2) Install UE4 4.13.

Windows Instructions

3) Install Visual Studio 2015 Community using custom install to include C++ development frameworks 
4) Right click HitboxDemo.uproject and select "Generate Visual Studio project files." This will create the HitboxDemo.sln 
5) Open HitboxDemo.sln and click "Build HitboxDemo" 6) Open HitboxDemo.uproject to navigate the project assets

OSX Instructions

3) Install latest version of Xcode 
4) Right click HitboxDemo.uproject and select "Services->Generate Xcode Project" to integrate with Xcode. This will create the HitboxDemo.xcworkspace....which I haven't successfully gotten to compile and run, but maybe some day...